import React from 'react'

import './task-list.css'

export default function TaskList({ taskArr, onDeleteClick, onDoneClick }) {
    return (
        <div>
            <ul>
                {taskArr.map((item, index) =>
                    <li key={index} className='task-item-container'>
                        <div
                            className='item-done'
                            onClick={() => onDoneClick(index)}
                        >
                            <i className={item.active ? "far fa-square" : "far fa-check-square" }></i>
                        </div>
                        <div className={item.active ? 'item-text': 'item-text-done'}>{item.text}</div>
                        <div
                            className='item-delete'
                            onClick={() => onDeleteClick(index)}
                        >
                            <i className="far fa-trash-alt"></i>
                        </div>
                    </li>)}
            </ul>
        </div>)
}