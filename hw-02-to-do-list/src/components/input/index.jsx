import React, { Component } from 'react'

import './input.css'

export default class Input extends Component {

    state = {
        inputText: ''
    }

    inputTask = (text) => {
        this.setState({
            inputText: text
        })
    }
    render() {

        const onAddBtnClick = () => {
            const a = this.state.inputText.length < 4 ? null : this.props.onUpdateTaskArr({
                text: this.state.inputText,
                date: new Date(),
                active: true
            });
            this.setState({
                inputText: ""
            })
        }

        return (
            <div>
                <input
                    className='input-task'
                    value={this.state.inputText}
                    onInput={(event) => this.inputTask(event.target.value)}
                >
                </input>
                <input
                    type='button'
                    className='add-button'
                    value="Add"
                    onClick={() => onAddBtnClick()}
                >
                </input>
            </div>
        )
    }
}
