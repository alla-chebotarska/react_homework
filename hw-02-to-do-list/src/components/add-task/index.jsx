import React, { Component } from 'react'
import Input from '../input'

export default function AddTask(props) {
    return (
        <div>
            <Input onUpdateTaskArr={props.onUpdateTaskArr}></Input>
        </div>
    )
}
