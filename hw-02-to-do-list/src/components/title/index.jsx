import React from 'react'

import './title.css'

export default function Title({text}) {
    return (
        <div>
            <h3 className='title'>{text}</h3>
        </div>
    )
}
