import React, { Component } from 'react'
import AddTask from '../add-task';
import TaskList from '../task-list';
import Title from '../title';

export default class Main extends Component {

    state = {
        taskArr: []
    }

    onUpdateTaskArr = (item) => {
        let updatedTaskArr = this.state.taskArr;
        updatedTaskArr.push(item);
        this.setState({
            taskArr: updatedTaskArr
        })
    }

    onDoneClick = (index) => {
        let updatedTaskArr = this.state.taskArr;
        updatedTaskArr[index].active = !this.state.taskArr[index].active;
        this.setState({
            taskArr: updatedTaskArr
        })
    }

    onDeleteClick = (index) => {
        let updatedTaskArr = this.state.taskArr;
        updatedTaskArr.splice(index, 1);
        this.setState({
            taskArr: updatedTaskArr
        })
    }

    countActiveTask = (taskArr) => {
        let count = 0;
        taskArr.forEach(task => {
            if(task.active === true)
            count++
        });
        return count;
    }

    render() {
        return (
            <div>
                <Title text="List of task for today"></Title>
                <AddTask onUpdateTaskArr={this.onUpdateTaskArr}></AddTask>
                <TaskList
                    taskArr={this.state.taskArr}
                    onDoneClick={this.onDoneClick}
                    onDeleteClick={this.onDeleteClick}>
                </TaskList>
                <Title text={`${this.countActiveTask(this.state.taskArr)} task left`}></Title>
            </div>
        )
    }
}
