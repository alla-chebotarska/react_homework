import React, { Component } from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';
import EditIcon from '@material-ui/icons/Edit';

import './section.css';

import { data } from '../reques';

export default class Section extends Component {

    state = {
        allCurrency: null,
        error: false
    }

    componentWillReceiveProps(nextProps) {
        this.dataDownload(nextProps.url);
    }

    componentDidMount() {
        this.dataDownload(this.props.url);
    }

    dataDownload(props) {
        data(props)
            .then((data1) => {
                this.setState(({ allCurrency, error }) => {
                    return {
                        allCurrency: data1,
                        error: false
                    } // повертає новий об'єкт
                })
            })
            .catch(error => {
                console.log("Failed to load:", error);
                this.setState(({error}) => {
                    return {
                        error: true
                    }
                })
            });
    }

    onEditBtnClick = (index) => {
        const { allCurrency } = this.state;
        let newCurrencyName = prompt("Enter new currency name");
        let newPrice = prompt("Enter new price");
        if (newCurrencyName === null || newCurrencyName === "") {
            newCurrencyName = allCurrency[index].txt;
        } else {
            let newAllCurrency = allCurrency;
            newAllCurrency[index].txt = newCurrencyName;
            this.setState(({ allCurrency }) => {
                return {
                    allCurrency: newAllCurrency
                }
            })
        }

        //edid Price
        if (newPrice === null || newPrice === "" || isNaN(newPrice)) {
            allCurrency[index].rate ? newPrice = allCurrency[index].rate : newPrice = allCurrency[index].value;
        } else {
            let newAllCurrency = allCurrency;
            allCurrency[index].rate ? allCurrency[index].rate = parseInt(newPrice) : allCurrency[index].value = parseInt(newPrice);
            this.setState(({ allCurrency }) => {
                return {
                    allCurrency: newAllCurrency
                }
            })
        }
        alert(`You provide changes to ${allCurrency[index].txt}. Current price  was changed to ${newPrice}`)

    }

    render() {
        function leadingZero(time) {
            if(time <= 9){
                time = "0" + time;
            }
            return time;
        }

        const { allCurrency, error } = this.state;
        const fullDate = new Date();
        const year = fullDate.getFullYear();
        const mounth =fullDate.getMonth() + 1;
        const date = fullDate.getDate();
        const hours = fullDate.getHours();
        const minutes = fullDate.getMinutes();
        const currentDate = year.toString() + "." + leadingZero(mounth.toString()) + "." + leadingZero(date.toString()) + " " + leadingZero(hours.toString()) + ":" + leadingZero(minutes.toString());

        return (
            <div>
                <h3>Курс на {currentDate} </h3>
                {!error ? 
                <table className='table-container'>
                    <tbody>
                        <tr>
                            <th>Currency name</th>
                            <th>Price</th>
                            <th>Cod</th>
                            <th>Edit</th>
                        </tr>
                        {Array.isArray(allCurrency) ?
                            allCurrency.map((item, index) => {
                                return (
                                    <tr key={index + "t"}>
                                        <td>{item.txt}</td>
                                        <td>{item.rate ? item.rate.toFixed(2) : item.value.toFixed(2)}</td>
                                        <td>{item.cc}</td>
                                        <td>
                                            <EditIcon
                                                className="edit-icon"
                                                onClick={() => this.onEditBtnClick(index)}
                                            />
                                        </td>
                                    </tr>
                                )
                            }) : <tr><td colSpan='4' className='progress-spiner'><CircularProgress /></td></tr>}
                    </tbody>
                </table> :
                <div className="error">Failed to fetch data</div>}
            </div>
        )
    }
}
