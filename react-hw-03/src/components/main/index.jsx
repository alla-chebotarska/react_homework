import React, { Component } from 'react';
import Section from '../section';
import Home from '../home';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

import './main.css';


class Main extends Component {

    render() {

        // перший спосіб створення запиту
        /*let promise = fetch('https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json')
        promise.then((data) => {return data.json()})
        .then((data1)=> {console.log(data1)})
        */
        return (
            <Router>
                <header>
                    <Link
                        to="/"
                        className='navigation-link'>
                        Главная
                        </Link>
                    <Link
                        to='/currency'
                        className='navigation-link'>
                        Офіційний курс гривні до іноземних валют та облікова ціна банківських металів
                    </Link>
                    <Link
                        to='/bank-info'
                        className='navigation-link'>
                        Основні показники діяльності банків України
                    </Link>
                </header>
                <Switch>
                    <Route exact path='/'>
                        <Home></Home>
                    </Route>
                    <Route path='/currency'>
                        <Section url='https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json'></Section>
                    </Route>
                    <Route path='/bank-info'>
                        <Section url="https://bank.gov.ua/NBUStatService/v1/statdirectory/basindbank?date=20160101&period=m&json"></Section>
                    </Route>
                </Switch>
            </Router>
        )
    }

}
export default Main;