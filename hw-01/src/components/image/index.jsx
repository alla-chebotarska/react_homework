import React from 'react';

import './image.css';

import mainImage from './mainImage.png';

export default function Image() {
    return (
        <div className='image-container'>
            <img
                src={mainImage}
                className='main-img'>
            </img>
        </div>
    )
}
