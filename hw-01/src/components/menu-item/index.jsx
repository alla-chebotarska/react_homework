import React from 'react';

import './menu-item.css';

export default function MenuItem({ text }) {
    return (
        <li className='menu-item'>{text}</li>
    )
}
