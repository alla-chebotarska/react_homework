import React from 'react';

import './article-header.css';

export default function ArticleHeader({articleHeader}) {
    return (
        <div>
            <h3 className='article-header'>{articleHeader}</h3>
        </div>
    )
}
