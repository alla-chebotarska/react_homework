import React from 'react';
import MenuItem from '../menu-item';
import Image from '../image';
import Article from '../article';
import Button from '../button';

import './main.css'
import Footer from '../../footer';


export default function Main() {
    return (
        <div className='main'>
            <ul id='menu'>
                <MenuItem text='home'></MenuItem>
                <MenuItem text='photoapp'></MenuItem>
                <MenuItem text='design'></MenuItem>
                <MenuItem text='download'></MenuItem>
            </ul>
            <div className='page-content'>
                <Image></Image>
                <Article></Article>
                <Button buttonText='Get started'></Button>
            </div>
            <Footer footerContent='Copyright by phototime - all right reserved'></Footer>

        </div>
    )
}
