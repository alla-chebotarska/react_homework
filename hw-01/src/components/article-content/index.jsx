import React from 'react';

import './article-content.css';

export default function ArticleContent({articleContent}) {
    return (
        <div>
            <p className='article-content'>{articleContent}</p>
        </div>
    )
}
