import React from 'react';

import './button.css';

export default function Button({ buttonText }) {
    return (
        <div className='button-container'>
            <input
                type='button'
                value={buttonText}
                className='button'>
            </input>
        </div>
    )
}
