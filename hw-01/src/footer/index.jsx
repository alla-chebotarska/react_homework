import React from 'react';

import './footer.css';

export default function Footer({footerContent}) {
    return (
        <div>
            <p className='footer'>{footerContent}</p>
        </div>
    )
}
