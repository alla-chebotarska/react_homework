function reducer(action, state, value) {
  switch (action.type) {
    case "NUMBER":
      if (state === 0) {
        newState = value;
        stack.push(value); // first number in display should display without zero in front 
      } else {
        if (operands.includes(stack[stack.length - 1])) {
          newState = value;
          stack.push(value);
        } else {
          newState = state + value;
          stack.push(value);
        }
      }
      break;
    case "OPERAND":
      if (operands.includes(stack[stack.length - 1])) {  //check if button with operand was clicked more than once
        stack.splice(stack.length - 1, 1, value);
      } else {
        stack.push(value);
      }
      break;
    case "EQUAL":
      newState = stack.join("");
      break;
    case "CANCEL":
      newState = 0;
      stack = [];
  }
  return newState;
}
let state = 0;
let newState = "";
let stack = [];
const operands = ["+", "-", "*", "/"];

const display = document.getElementById("display");

//Numbers
const zero = document.getElementById("zero");
const one = document.getElementById("one");
const two = document.getElementById("two");
const three = document.getElementById("three");
const four = document.getElementById("four");
const five = document.getElementById("five");
const six = document.getElementById("six");
const seven = document.getElementById("seven");
const eight = document.getElementById("eight");
const nine = document.getElementById("nine");

//OperandssetDisplayAtribute(newState);
const plus = document.getElementById("plus");
const minus = document.getElementById("minus");
const multiply = document.getElementById("multiply");
const divide = document.getElementById("divide");

//Equal
const equal = document.getElementById("equal");

const cancel = document.getElementById("cancel");

function setDisplayAtribute(value) {
  display.setAttribute("value", value);
}

// Event Listeners for digits buttons
zero.addEventListener("click", () => {
  state = reducer({ type: "NUMBER" }, state, zero.value);
  setDisplayAtribute(newState);
});

one.addEventListener("click", () => {
  state = reducer({ type: "NUMBER" }, state, one.value);
  setDisplayAtribute(newState);
});

two.addEventListener("click", () => {
  state = reducer({ type: "NUMBER" }, state, two.value);
  setDisplayAtribute(newState);
});

three.addEventListener("click", () => {
  state = reducer({ type: "NUMBER" }, state, three.value);
  setDisplayAtribute(newState);
});

four.addEventListener("click", () => {
  state = reducer({ type: "NUMBER" }, state, four.value);
  setDisplayAtribute(newState);
});

five.addEventListener("click", () => {
  state = reducer({ type: "NUMBER" }, state, five.value);
  setDisplayAtribute(newState);
});

six.addEventListener("click", () => {
  state = reducer({ type: "NUMBER" }, state, six.value);
  setDisplayAtribute(newState);
});

seven.addEventListener("click", () => {
  state = reducer({ type: "NUMBER" }, state, seven.value);
  setDisplayAtribute(newState);
});

eight.addEventListener("click", () => {
  state = reducer({ type: "NUMBER" }, state, eight.value);
  setDisplayAtribute(newState);
});

nine.addEventListener("click", () => {
  state = reducer({ type: "NUMBER" }, state, nine.value);
  setDisplayAtribute(newState);
});

// Event Listeners for operands buttons
plus.addEventListener("click", () => {
  state = reducer({ type: "OPERAND" }, state, plus.value);
  setDisplayAtribute(newState);
});

minus.addEventListener("click", () => {
  state = reducer({ type: "OPERAND" }, state, minus.value);
  setDisplayAtribute(newState);
});

multiply.addEventListener("click", () => {
  state = reducer({ type: "OPERAND" }, state, multiply.value);
  setDisplayAtribute(newState);
});

divide.addEventListener("click", () => {
  state = reducer({ type: "OPERAND" }, state, divide.value);
  setDisplayAtribute(newState);
});

// Event Listener for equal button

equal.addEventListener("click", () => {
  state = reducer({ type: "EQUAL" }, state, equal.value);
  setDisplayAtribute(newState);
})

cancel.addEventListener("click", () => {
  state = reducer({ type: "CANCEL" }, state, cancel.value);
  setDisplayAtribute(newState);
})